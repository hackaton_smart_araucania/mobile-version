import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:turismo_app/widget/font_styles.dart';
import 'package:turismo_app/widget/list_card_popular.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:turismo_app/widget/notice.dart';
import 'info.dart';


class Home extends StatefulWidget{

  @override
  _HomeState createState() => _HomeState();
}
class _HomeState extends State<Home>{
  String barcode;
  @override
  void initState(){
    super.initState();
  }

  Future scanCode() async {
    barcode = await FlutterBarcodeScanner.scanBarcode("#004297", "Cancelar", true);
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {

    final title = Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.only(left: 15, top: 5),
      child:Text("Populares", style: FontStyles.fontStyleBlack)
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(

        automaticallyImplyLeading: false,
        title: Text("Inicio", style: FontStyles.fontStyleWhite),

        actions: <Widget>[

          IconButton(
            onPressed: (){},
            icon: Icon(Icons.search),
          ),

          IconButton(
            onPressed: scanCode,
            icon: Icon(Icons.camera_alt),
          ),

        ],
      ),

      body: SingleChildScrollView(

        child: Column(

          children: <Widget>[
            title,
            ListCardPopular(),
            Notice(),

          ],
        ),

      ),
    );
  }

}