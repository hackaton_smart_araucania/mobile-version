import 'package:flutter/material.dart';
import 'package:turismo_app/widget/card_info.dart';
import 'package:turismo_app/widget/font_styles.dart';

import 'comentary.dart';

class Info extends StatefulWidget{
  @override
  _InfoState createState() => _InfoState();
}

class _InfoState extends State<Info>{
  @override
  Widget build(BuildContext context) {



    return Scaffold(

      appBar: AppBar(
        title: Text("Informacion", style: FontStyles.fontStyleWhite),

      ),

      body: Container(
        child: Column(
          children: <Widget>[
            CardInfo("assets/img/Cerro.jpg","Cerro Ñielol", "Temuco", "Lorem ipsum dolor sit amet consectetur adi elit iaculis, elementum maecenas sem veh litora turpis vestibulum. felis dis cubilia id ligula fermentum phas eu, duis iaculis semper nisi."),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Comentary()),
          );
        },
        label: Text('Ver Comentarios'),
        icon: Icon(Icons.comment),
        backgroundColor: Colors.pink,
      ),
    );

  }


}