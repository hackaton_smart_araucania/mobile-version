import 'package:flutter/material.dart';

import 'comentary.dart';

class NewComentary extends StatefulWidget{
  @override
  _NewComentaryState createState() => _NewComentaryState();
}
class _NewComentaryState extends State<NewComentary>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Nuevo Comentario",
          style: TextStyle(
              fontFamily: "Poppins",
              fontSize: 22,
              color: Colors.white,
              fontWeight: FontWeight.w900
          ),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 5, right: 5, top: 20),
              child:TextFormField(
                style: TextStyle(
                  fontFamily: "Poppins",
                  fontSize: 22,
                  color: Colors.black54,
                  fontWeight: FontWeight.w900,
                ),
                decoration: InputDecoration(
                  hintText: "Nombre",
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 5, right: 5, top: 20),
              child:TextFormField(
                style: TextStyle(
                  fontFamily: "Poppins",
                  fontSize: 22,
                  color: Colors.black54,
                  fontWeight: FontWeight.w900,
                ),
                decoration: InputDecoration(
                  hintText: "Comentario",
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Comentary()),
          );
        },
        label: Text('Enviar'),
        icon: Icon(Icons.add),
        backgroundColor: Colors.pink,
      ),
    );
  }

}