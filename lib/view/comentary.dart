import 'package:flutter/material.dart';
import 'package:turismo_app/widget/info_list_review.dart';
import 'package:turismo_app/view/newcomentary.dart';

class Comentary extends StatefulWidget{
  @override
  _ComentaryState createState() => _ComentaryState();
}

class _ComentaryState extends State<Comentary>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Comentarios",
          style: TextStyle(
              fontFamily: "Poppins",
              fontSize: 22,
              color: Colors.white,
              fontWeight: FontWeight.w900
          ),
        ),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            InfoListReview(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => NewComentary()),
          );
        },
        label: Text('Comentar'),
        icon: Icon(Icons.comment),
        backgroundColor: Colors.pink,
      ),
    );
  }

}