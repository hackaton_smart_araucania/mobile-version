import 'package:flutter/material.dart';

class FontStyles{

  static TextStyle fontStyleWhite = TextStyle(
      fontFamily: 'Poppins',
      fontSize: 22.0,
      color: Colors.white,
      fontWeight: FontWeight.w900
  );

  static TextStyle fontStyleBlack = TextStyle(
      fontFamily: 'Poppins',
      fontSize: 25.0,
      color: Colors.black,
      fontWeight: FontWeight.w900
  );

  static TextStyle fontStyleBlue = TextStyle(
      fontFamily: 'Poppins',
      fontSize: 17.0,
      color: Colors.blue,
      fontWeight: FontWeight.w900
  );

  static TextStyle fontStyleBlack2 = TextStyle(
      fontFamily: 'Poppins',
      fontSize: 22.0,
      color: Colors.black,
      fontWeight: FontWeight.w900
  );

  static TextStyle fontStyleBlack3 = TextStyle(
      fontFamily: 'Poppins',
      fontSize: 20.0,
      color: Colors.black,
      fontWeight: FontWeight.w900
  );

  static TextStyle fontStyleCity = TextStyle(
    fontFamily: 'Poppins',
    fontSize: 15.0,
    color: Colors.black38,
  );

  static TextStyle fontStyleDescription = TextStyle(
      fontFamily: 'Poppins',
      fontSize: 20.0,
      color: Colors.black38,
  );

  static TextStyle fontStyleDescription2 = TextStyle(
    fontFamily: 'Poppins',
    fontSize: 15.0,
    color: Colors.black38,
  );



}