import 'package:flutter/material.dart';
import 'package:turismo_app/widget/info_review.dart';
import 'package:flutter/cupertino.dart';

class InfoListReview extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: ListView(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.only(left: 15, right: 15 ),
        children: <Widget>[
          InfoReview("Diego Aguero", "Lorem ipsum dolor sit amet consectetur adipi elit, ante at proin a risus cras, himenaeos ferm dis ultricies justo platea."),
          InfoReview("Cristobal Carrion", "Lorem ipsum dolor sit amet consectetur adipi elit, ante at proin a risus cras, himenaeos ferm dis ultricies justo platea."),
          InfoReview("Eduardo Queupumil", "Lorem ipsum dolor sit amet consectetur adipi elit, ante at proin a risus cras, himenaeos ferm dis ultricies justo platea."),
          InfoReview("Valentina Villegas", "Lorem ipsum dolor sit amet consectetur adipi elit, ante at proin a risus cras, himenaeos ferm dis ultricies justo platea."),

        ],
      ),
    );
  }

}