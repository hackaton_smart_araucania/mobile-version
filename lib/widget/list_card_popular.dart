import 'package:flutter/material.dart';
import 'package:turismo_app/widget/card_image.dart';

class ListCardPopular extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 270.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          CardImage("assets/img/Cerro.jpg","Cerro Ñielol", "Temuco"),
          CardImage("assets/img/Dreams.jpg","Casino Dreams", "Temuco"),
          CardImage("assets/img/Museo.jpg","Museo Ferroviario", "Temuco"),
          CardImage("assets/img/Museo2.jpg","Museo El Amor de Chile", "Temuco"),
          CardImage("assets/img/Portal.jpg","Portal Temuco", "Temuco"),
        ],
      ),
    );
  }

}