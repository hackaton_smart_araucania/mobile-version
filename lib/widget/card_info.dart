import 'package:flutter/material.dart';
import 'package:turismo_app/view/info.dart';
import 'package:turismo_app/widget/floating_action_button_home.dart';
import 'package:turismo_app/widget/font_styles.dart';

// ignore: must_be_immutable
class CardInfo extends StatelessWidget{
  String pathImage = "";
  String titleInfo = "";
  String cityInfo = "";
  String descriptionInfo = "";
  CardInfo(this.pathImage, this.titleInfo, this.cityInfo, this.descriptionInfo);

  @override
  Widget build(BuildContext context) {

    final title = Container(
      alignment: Alignment.bottomLeft,
      margin: EdgeInsets.only(left: 20, top: 205),
      child: Text(titleInfo, style: FontStyles.fontStyleBlack2),
    );
    final city = Container(
      alignment: Alignment.bottomLeft,
      margin: EdgeInsets.only(left: 20, top: 230),
      child: Text(cityInfo, style: FontStyles.fontStyleCity),
    );

    final description = Container(
      alignment: Alignment.bottomLeft,
      margin: EdgeInsets.only(left: 20, top: 255, right: 10),
      child: Text(descriptionInfo, style: FontStyles.fontStyleCity),
    );

    final card = Container(
      alignment: Alignment.topCenter,
      height: 200,
      width: 330,
      margin: EdgeInsets.only(top: 5, left: 15),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(pathImage),
              fit: BoxFit.cover
          ),
          borderRadius: BorderRadius.circular(10.0),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )
          ]
      ),
    );

    final card2 = Container(
      child: Stack(

        children: <Widget>[
          card,
          title,
          city,
          description

        ],
      ),
    );
    return card2;
  }
}