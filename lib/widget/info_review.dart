import 'package:flutter/material.dart';

class InfoReview extends StatelessWidget{
  String nameReview = '';
  String descriptionReview = '';

  InfoReview(this.nameReview, this.descriptionReview);

  @override
  Widget build(BuildContext context) {
    final nameText = Container(
      alignment: Alignment.topLeft,
      child: Text(
        nameReview,
        style: TextStyle(
            fontFamily: "Poppins",
            fontSize: 22,
            color: Colors.black,
            fontWeight: FontWeight.w900
        ),
      ),
    );

    final descriptionText = Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 15.0),
      child: Text(
        descriptionReview,
        softWrap: true,
        style: TextStyle(
            fontFamily: "Poppins",
            fontSize: 17,
            color: Colors.black54,
            fontWeight: FontWeight.w900
        ),
      ),
    );

    return Container(
      child: Column(
        children: <Widget>[
          nameText,
          descriptionText,
        ],
      ),
    );
  }
}