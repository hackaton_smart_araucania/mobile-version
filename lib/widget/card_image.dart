import 'package:flutter/material.dart';
import 'package:turismo_app/view/info.dart';
import 'package:turismo_app/widget/floating_action_button_home.dart';
import 'package:turismo_app/widget/font_styles.dart';

// ignore: must_be_immutable
class CardImage extends StatelessWidget{
  String pathImage = "";
  String titleCard = "";
  String cityCard = "";
  CardImage(this.pathImage, this.titleCard, this.cityCard);

  @override
  Widget build(BuildContext context) {

    final title = Container(
      alignment: Alignment.bottomLeft,
      margin: EdgeInsets.only(left: 20, bottom: 32 ),
      child: Text(titleCard, style: FontStyles.fontStyleBlack2),
    );
    final city = Container(
      alignment: Alignment.bottomLeft,
      margin: EdgeInsets.only(left: 20, bottom: 18),
      child: Text(cityCard, style: FontStyles.fontStyleCity),
    );

    final viewmore = Container(
      alignment: Alignment.bottomLeft,
      margin: EdgeInsets.only(left: 20),
      child: InkWell(
        child: Text("Ver más",
            style: FontStyles.fontStyleBlue),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => Info()));
        },
      ),
    );




    final card = Container(
      height: 200,
      width: 300,
      margin: EdgeInsets.only(top: 5, left: 15),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(pathImage),
              fit: BoxFit.cover
          ),
          borderRadius: BorderRadius.circular(10.0),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )
          ]
      ),
    );



    final card2 = Container(
      child: Stack(

        children: <Widget>[
          card,
          Container(
            margin: EdgeInsets.only(left: 250, top: 160),

          ),
          viewmore,
          title,
          city,

        ],
      ),
    );
    return card2;
  }
}