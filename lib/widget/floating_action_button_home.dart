import 'package:flutter/material.dart';

class FloatingActionButtonHome extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _FloatingActionButtonHome();
  }

}

class _FloatingActionButtonHome extends State<FloatingActionButtonHome>{

  bool favorite = false;

  void onPressedButton(){
    setState(() {
      favorite = !favorite;
    });
    var mark = favorite ? "Marcado" : "Desmarcado";

    Scaffold.of(context).showSnackBar(
      SnackBar(content: Text("$mark como Favorito"))
    );
  }

  @override
  Widget build(BuildContext context) {
    return  FloatingActionButton(
      onPressed: onPressedButton,
      backgroundColor: Colors.red,
      child: favorite ? Icon(Icons.favorite): Icon(Icons.favorite_border),
    );
  }
}