import 'package:flutter/material.dart';
import 'package:turismo_app/widget/font_styles.dart';

class Notice extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(

      child: Column(
        children: <Widget>[

          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(left: 20, top: 20),
            child: Text(
              "Noticias",
              style: FontStyles.fontStyleBlack,
            ),
          ),

          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(left: 20),
            child: Text(
                "Titulo 27/10/2019 ",
              style: FontStyles.fontStyleBlack3,

            ),
          ),

          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Text(
                "Lorem ipsum dolor sit amet conse adi elit iaculis, elementum maecenas se veh litora turpis vestibulum. felis cubil id ligula fermentum phas eu.",
              style: FontStyles.fontStyleDescription2,

            ),
          ),

          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(left: 20, top: 20),
            child: Text(
              "Titulo 26/10/2019 ",
              style: FontStyles.fontStyleBlack3,

            ),
          ),

          Container(

            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Text(
              "Lorem ipsum dolor sit amet conse adi elit iaculis, elementum maecenas se veh litora turpis vestibulum. felis cubil id ligula fermentum phas eu.",
              style: FontStyles.fontStyleDescription2,

            ),
          ),

          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(left: 20, top: 20),
            child: Text(
              "Titulo 25/10/2019 ",
              style: FontStyles.fontStyleBlack3,

            ),
          ),

          Container(

            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Text(
              "Lorem ipsum dolor sit amet conse adi elit iaculis, elementum maecenas se veh litora turpis vestibulum. felis cubil id ligula fermentum phas eu.",
              style: FontStyles.fontStyleDescription2,

            ),
          ),
        ],
      ),
    );
  }

}